# Transspecies Pride Flag

![transspecies-pride-flag](./transspecies-pride-flag.png)

This is a pride flag for transspecies creatures!

If you don't know what that is and you're curious, we're basically like transgender critters except for species. Something not human – might be regular animals like wolves, fantasy animals like dragons, or even something else entirely!

## The Colors

These are the colors used in the flag:
- `#9040c0`
- `#d0d0d0`
- `#505050`
- `#40a080`
- `#2050a0`
- `#a08060`

## Pixel Art Version

There's a pixel art version for emoji and whatnot, too!

![pixel-art-flag@2x](./@2x/transspecies-pride-flag-pixel@2x.png)

<!-- the U+200B's here are zero-width spaces to make Gitea not interpret @1x/@2x as user links -->
You'll probably want to use the @​2x one for emoji, because the @​1x one will look bad on high DPI displays as it gets scaled up.

## License

It's all public domain, feel free to use it wherever!
