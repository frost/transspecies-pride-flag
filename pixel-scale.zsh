#!/bin/zsh -f

for file in @1x/*.png; do
	basename=${file##*/}
	basename=${basename%.png}
	convert ./$file -sample '200%' ./@2x/$basename@2x.png
	convert ./$file -sample 320x320 320px/$basename-320px.png
	convert ./$file -magnify 2xsai/$basename-2xsai.png
done
