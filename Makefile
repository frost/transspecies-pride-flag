png:
	inkscape -w 512 transspecies-pride-flag.svg -o transspecies-pride-flag.png

pixel: @1x/transspecies-pride-flag-pixel.png
	mkdir -p @2x/ 320px/ 2xsai/
	./pixel-scale.zsh

@1x/transspecies-pride-flag-pixel.png:
	mkdir -p @1x/
	krita transspecies-pride-flag.kra --export --export-filename @1x/transspecies-pride-flag-pixel.png
